package com.framework.testcases;

	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import com.framework.design.ProjectMethods;
	import com.framework.pages.LoginPage;

	public class TC002_CreateLead extends ProjectMethods{
		@BeforeTest
		public void setdata()
		{
			testCaseName="TC002_Create Lead";
			testDescription = "Login";
			testNodes ="Leads";
			author = "Mahes";
			category ="Smoke";
			dataSheetName="TC001";
			
		}
		
		@Test(dataProvider="fetchData")
		public void createLead(String username,String password,String firstname,String lastname,String companyname)
		{
			new LoginPage()
			.enterUsername(username)
			.enterPassword(password)
			.clickLogin()
			.clickCRMSFA()
			.clickLeads()
			.clickCreateLead()
			.enterFirstname(firstname)
			.enterLastName(lastname)
			.enterCompName(companyname)
			.submitCreateLead();
		}

	}



