package com.framework.testcases;


		import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import com.framework.design.ProjectMethods;
	import com.framework.pages.LoginPage;

	public class TC003_FindLead extends ProjectMethods{
		@BeforeTest
		public void setdata()
		{
			testCaseName="TC003_Find Lead";
			testDescription = "Login";
			testNodes ="Leads";
			author = "Mahes";
			category ="Smoke";
			dataSheetName="TC003";
			
		}
		
		@Test(dataProvider="fetchData")
		public void findLead(String username,String password,String companyname)
		{
			new LoginPage()
			.enterUsername(username)
			.enterPassword(password)
			.clickLogin()
			.clickCRMSFA()
			.clickLeads()
			.clickFindLead()
			//.enterFirstname(firstname)
			.firstNameSelect()
			.clickEdit()
			.UpdateCompanyName(companyname)
			.UpdateLead();
		}

	}




