package com.framework.pages;


	

	import org.openqa.selenium.WebElement;
	import org.openqa.selenium.support.FindBy;
	import org.openqa.selenium.support.How;
	import org.openqa.selenium.support.PageFactory;

	import com.framework.design.ProjectMethods;

	public class EditLeadPage extends ProjectMethods {
		
		public EditLeadPage() {
		       PageFactory.initElements(driver, this);
			}
		
		@FindBy(how = How.ID,using="updateLeadForm_companyName") WebElement eleUpdateCompanyname;
		@FindBy(how = How.NAME,using="submitButton") WebElement eleUpdate;
		
		
		
		public EditLeadPage UpdateCompanyName(String data) {
			//WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleUpdateCompanyname, data);
			return this;
		}
			
			public ViewLeadPage UpdateLead() {
				//WebElement eleLogout = locateElement("class", "decorativeSubmit");
				click(eleUpdate);
				return new ViewLeadPage();

	}

	}


