package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadsPage extends ProjectMethods {
	
	public LeadsPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement eleCreateLead;
		@FindBy(how = How.LINK_TEXT,using="Find Leads") WebElement eleFindLead;
		
		public CreateLeadPage clickCreateLead() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleCreateLead);
			return new CreateLeadPage();
		}
		public FindLeadPage clickFindLead() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleFindLead);
			return new FindLeadPage();
		}

}
