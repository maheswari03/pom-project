package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	public CreateLeadPage() {
	       PageFactory.initElements(driver, this);
		}
	
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstname;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompName;
	@FindBy(how = How.NAME,using="submitButton") WebElement eleSubmitCreate;
	
	public CreateLeadPage enterFirstname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleFirstname, data);
		return this; 
	}
	public CreateLeadPage enterLastName(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleLastName, data);
		return this;
	}
	public CreateLeadPage enterCompName(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleCompName, data);
		return this;
	}

		
		public ViewLeadPage submitCreateLead() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleSubmitCreate);
			return new ViewLeadPage();

}
}
