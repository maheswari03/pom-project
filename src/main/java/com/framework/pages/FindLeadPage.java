package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadPage extends ProjectMethods {
	
	public FindLeadPage() {
	       PageFactory.initElements(driver, this);
		}
	
	@FindBy(how = How.NAME,using="firstName") WebElement eleFirstname;
	@FindBy(how = How.XPATH,using="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']//a") WebElement eleFirstnameSelect;
	@FindBy(how = How.LINK_TEXT,using="Edit") WebElement eleEdit;
	
	
	public FindLeadPage enterFirstname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleFirstname, data);
		return this; 
	}
	public FindLeadPage firstNameSelect() {
		//WebElement elePassword = locateElement("id", "password");
	click(eleFirstnameSelect);
		return this;
	}
	public EditLeadPage clickEdit() {
		//WebElement elePassword = locateElement("id", "password");
		click(eleEdit);
		return new EditLeadPage();
	}
	}
